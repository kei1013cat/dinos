# coding:utf-8

import paramiko
import os
import shutil

HOST = 'hearty-sapporo.sakura.ne.jp'
USER = 'hearty-sapporo'
PSWD = 'ax6p6pyr87'

LOCAL_PATH  = r"./output/配信分/"
REMOTE_PATH = "/home/hearty-sapporo/www/cinema-test/"

def get_date():
	fin=open('date.txt')
	line=fin.read()
	fin.close()
	return line

def del_file():
	fin=open('date.txt')
	line=fin.read()
	fin.close()
	return line
 
FILE_DATE = get_date()

ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh.connect(HOST, username=USER, password=PSWD)
 
sftp = ssh.open_sftp()
sftp.mkdir(REMOTE_PATH + FILE_DATE)

sftp.put(LOCAL_PATH + "sapporo.html" , REMOTE_PATH + FILE_DATE + "/sapporo.html")
sftp.put(LOCAL_PATH + "asahikawa.html" , REMOTE_PATH + FILE_DATE + "/asahikawa.html")
sftp.put(LOCAL_PATH + "tomakomai.html" , REMOTE_PATH + FILE_DATE + "/tomakomai.html")
sftp.put(LOCAL_PATH + "muroran.html" , REMOTE_PATH + FILE_DATE + "/muroran.html")

sftp.close()
ssh.close()

#os.remove('date.txt')
#os.remove('chinema_config.php')
#shutil.rmtree("./output/配信分")
#os.rename("./output/配信分","./output/配信分_" + FILE_DATE)
