<?php
$shop = "sapporo";
if(isset($_GET['shop'])){
    $shop = $_GET['shop'];
}

/*
【映画作品の画像URL】
■札幌

■旭川
http://cinema.sugai-dinos.jp/pc/asahikawa/coming_soon/index.php
■苫小牧
http://cinema.sugai-dinos.jp/pc/tomakomai/coming_soon/index.php
■室蘭
http://cinema.sugai-dinos.jp/pc/muroran/coming_soon/index.php
*/

// 映画作品一覧
include('./chinema_config.php');

//****************************************************************************************

$week_list = array("日", "月", "火", "水", "木", "金", "土");

$shop_name = array(
    'sapporo'   => '札幌劇場',
    'asahikawa' => '旭川',
    'tomakomai' => '苫小牧',
    'muroran'   => '室蘭'
);
$shop_addr = array(
    'sapporo'   => '札幌市中央区南3条西1丁目8番地',
    'asahikawa' => '旭川市大雪通5丁目ディノス旭川3F',
    'tomakomai' => '苫小牧市柳町3丁目1番20号イオンモール苫小牧2F',
    'muroran'   => '室蘭市東町4丁目31番3号'
);
$shop_img = array(
    'sapporo'   => 'https://www.sugai-dinos.jp/cinema/mailmagazine//top_menu_sapporo.png',
    'asahikawa' => 'https://www.sugai-dinos.jp/cinema/mailmagazine//top_menu_asahikawa.png',
    'tomakomai' => 'https://www.sugai-dinos.jp/cinema/mailmagazine//top_menu_tomakomai.png',
    'muroran'   => 'https://www.sugai-dinos.jp/cinema/mailmagazine//top_menu_muroran.png'
);

$shop_tw = array(
    'sapporo' => array(
                    'img' => 'https://www.sugai-dinos.jp/cinema/mailmagazine//bn/twitter_banner.jpg',
                    'url' => 'https://twitter.com/satsugeki_'
                ),
    'asahikawa' => array(
                    'img' => 'https://www.sugai-dinos.jp/cinema/mailmagazine//bn/twitter_banner_l_asahikawa.jpg',
                    'url' => 'https://twitter.com/DCasahikawa'
                ),
    'tomakomai' => array(
                    'img' => 'https://www.sugai-dinos.jp/cinema/mailmagazine//bn/twitter_banner_l_tomakomai.jpg',
                    'url' => 'https://twitter.com/DCtomakomai'
                ),
    'muroran' => array(
                    'img' => 'https://www.sugai-dinos.jp/cinema/mailmagazine//bn/twitter_banner_l_muroran.jpg',
                    'url' => 'https://twitter.com/DCmuroran'
                ),
);

$hillvalley_url = array(
    'sapporo'   => 'http://cinema.sugai-dinos.jp/pc/sapporo/topics/detail.php?id=13779',
    'asahikawa' => 'http://cinema.sugai-dinos.jp/pc/asahikawa/topics/detail.php?id=13780',
    'tomakomai' => 'http://cinema.sugai-dinos.jp/pc/tomakomai/topics/detail.php?id=13781',
    'muroran' => 'http://cinema.sugai-dinos.jp/pc/muroran/topics/detail.php?id=13782'
);

$godiva_url = array(
    'sapporo'   => 'http://cinema.sugai-dinos.jp/pc/sapporo/topics/detail.php?id=13517',
    'asahikawa' => 'http://cinema.sugai-dinos.jp/pc/asahikawa/topics/detail.php?id=13344',
    'tomakomai' => 'http://cinema.sugai-dinos.jp/pc/tomakomai/topics/detail.php?id=13131'
);

$chocolixir_url = array(
  'asahikawa' => 'http://cinema.sugai-dinos.jp/pc/asahikawa/topics/detail.php?id=13967',
  'tomakomai' => 'http://cinema.sugai-dinos.jp/pc/tomakomai/topics/detail.php?id=13927'

);

$medal_url = array(
  'sapporo'   => 'http://cinema.sugai-dinos.jp/mail/images/medal_bnr_free.jpg',
  'asahikawa' => 'http://cinema.sugai-dinos.jp/mail/images/medal_bnr_free.jpg',
  'tomakomai' => 'http://cinema.sugai-dinos.jp/mail/images/medal_bnr_free.jpg',
  'muroran' => 'http://cinema.sugai-dinos.jp/mail/images/crane_bnr_free.jpg'
);

$gurumaki_url = array(
  'muroran' => 'http://cinema.sugai-dinos.jp/pc/muroran/topics/detail.php?id=14491'
);

$medal_url = array(
  'sapporo'   => 'http://cinema.sugai-dinos.jp/mail/images/medal_bnr_free.jpg',
  'asahikawa' => 'http://cinema.sugai-dinos.jp/mail/images/medal_bnr_free.jpg',
  'tomakomai' => 'http://cinema.sugai-dinos.jp/mail/images/medal_bnr_free.jpg',
  'muroran' => 'http://cinema.sugai-dinos.jp/mail/images/crane_bnr_free.jpg'
);

$cremiachoco_url = array(
  'asahikawa' => 'https://www.sugai-dinos.jp/cinema/asahikawa/topics/2019/03/08/12/',
  'tomakomai' => 'https://www.sugai-dinos.jp/cinema/tomakomai/topics/2019/03/08/14/'
);

$cremiachoco_date = array(
  'asahikawa' => '3/6',
  'tomakomai' => '3/14'
);
$cremiachoco_amt = array(
  'asahikawa' => '500',
  'tomakomai' => '600'
);

$cremiasoftcream_url = array(
  'muroran' => 'https://www.sugai-dinos.jp/cinema/tomakomai/topics/2019/03/08/14/'
);
$cremiasoftcream_amt = array(
  'muroran' => '500'
);


?>
<html lang="ja">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>メルマガ(<?php echo $shop_name[$shop]; ?>)<?php echo $senddate->format('Y/n/j'); ?>配信</title>
</head>

<body>

    <table cellpadding="0" cellspacing="0" border="0" width="420">

        <!--シネマ -->
        <?php include('./include/cinema.php'); ?>



        <!-- アカデミー賞上映スケジュール -->
        <?php if(in_array($shop,array())):?>
        <?php include('./include/academy.php'); ?>
        <?php endif; ?>

        <!-- クレミア チョコレート -->
        <?php if(in_array($shop,array('tomakomai','asahikawa'))):?>
        <?php include('./include/cremia_choco.php'); ?>
        <?php endif; ?>

        <!-- クレミア プレミアムソフトクリーム -->
        <?php if(in_array($shop,array('muroran'))):?>
        <?php include('./include/cremia_softcream.php'); ?>
        <?php endif; ?>


        <!--　シネマスタンプラリー -->
        <?php if(in_array($shop,array())):?>
        <?php include('./include/stamprally.php'); ?>
        <?php endif; ?>

        <!-- ホットショコリキサー -->
        <?php if(in_array($shop,array())):?>
        <?php include('./include/hot-chocolixir.php'); ?>
        <?php endif; ?>

        <!-- アイスショコリキサー -->
        <?php if(in_array($shop,array('tomakomai','asahikawa'))):?>
        <?php //include('./include/ice-chocolixir.php'); ?>
        <?php endif; ?>

        <!--　ヒルバレー -->
        <?php if(in_array($shop,array())):?>
        <?php include('./include/hillvalley.php'); ?>
        <?php endif; ?>

        <!--ゴディバ -->
        <?php if(in_array($shop,array())):?>
        <?php //include('./include/godiva.php'); ?>
        <?php endif; ?>

        <!--オーガニックコーヒー -->
        <?php if(in_array($shop,array('sapporo'))):?>
        <?php include('./include/organic_coffee.php'); ?>
        <?php endif; ?>

        <!--ぐる巻きソーセージ -->
        <?php if(in_array($shop,array('muroran'))):?>
        <?php //include('./include/gurumaki.php'); ?>
        <?php endif; ?>

        <!--ポップコーンBOX -->
        <?php if(in_array($shop,array())):?>
        <?php include('./include/dorapop.php'); ?>
        <?php endif; ?>

        <!--ライザップLINEスタンプ -->
        <?php if(in_array($shop,array())):?>
        <?php include('./include/rizap_linestamp.php'); ?>
        <?php endif; ?>


        <!--フッター -->
        <?php include('./include/footer.php'); ?>

    </table>
</body>

</html>
