<!--　START ゴディバ -->
<tr>
  <td align="center" style="font-size:17px;font-weight:bold;padding:10px 0 15px;"> 新メニュー登場！<br> ＜ゴディバ ショコリキサー＞<br> 好評販売中です！</td>
</tr>
<tr>
  <td style="border:1px solid #666;"><table cellpadding="0" cellspacing="0" border="0" width="420">
      <tbody>
        <tr>
          <td align="center" bgcolor="#002060" style="color:#fff;padding:7px 0 6px;font-size:17px;"><strong>深い味わいの贅沢な２種類の<br>チョコレートドリンクです</strong></td>
        </tr>
        <tr>
          <td align="center" style="padding:15px 0;"><img src="https://www.sugai-dinos.jp/cinema/mailmagazine//godiva01.jpg"></td>
        </tr>
        <tr>
          <td align="center"><table cellpadding="0" cellspacing="0" border="0" width="120">
              <tbody>
                <tr>
                  <td align="center" bgcolor="#002060" style="color:#fff;padding:1px 0; font-size:13px;">ミルクチョコレートデカダンス</td>
                </tr>
              </tbody>
            </table></td>
        </tr>
        <tr>
          <td align="center" style="padding:8px 10px; font-size:13px;">ミルクチョコレートを使った、 適度な甘みでまろやかな味わい。ゴディバならではの一品です。</td>
        </tr>
        <tr>
          <td align="center"><table cellpadding="0" cellspacing="0" border="0" width="120">
              <tbody>
                <tr>
                  <td align="center" bgcolor="#002060" style="color:#fff;padding:1px 0; font-size:13px;">ダークチョコレートデカダンス</td>
                </tr>
              </tbody>
            </table></td>
        </tr>
        <tr>
          <td align="center" style="padding:8px 10px; font-size:13px;">カカオが香る、濃厚なダークチョコレート味。チョコレートソースをトッピングしました。</td>
        </tr>
        <tr>
          <td align="center"><table cellpadding="0" cellspacing="0" border="0" width="120">
              <tbody>
                <tr>
                  <td align="center" bgcolor="#002060" style="color:#fff;padding:1px 0; font-size:13px;">販売価格</td>
                </tr>
                <tr>
                  <td align="center" style="padding:8px 10px; font-size:13px;">600円（税込）</td>
                </tr>
                <tr>
                  <td align="center" style="padding:8px 10px; font-size:13px;"><a href="<?php echo $godiva_url[$shop]; ?>" target="_blank">⇒詳細はこちら</a></td>
                </tr>
              </tbody>
            </table></td>
        </tr>
      </tbody>
    </table></td>
</tr>
<tr>
  <td>&nbsp;</td>
</tr>
<!--　END ゴディバ -->