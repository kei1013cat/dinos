<!--　START オーガニックコーヒー -->
<tr>
  <td align="center" style="font-size:17px;font-weight:bold;padding:10px 0 15px;"> 「森彦」のオーガニックコーヒーをぜひ劇場で！</td>
</tr>
<tr>
  <td style="border:1px solid #666;"><table cellpadding="0" cellspacing="0" border="0" width="420">
      <tbody>
        <tr>
          <td align="center" bgcolor="#002060" style="color:#fff;padding:7px 0 6px;font-size:17px;"><strong>新たなシネマ＆カフェカルチャーの発信地で、<br>映画とコーヒーと空間をお楽しみください。</strong></td>
        </tr>
        <tr>
          <td align="center" style="padding:15px 0;"><img style="width:420;" src="http://cinema.sugai-dinos.jp/sapporo/c3/images/about1_photo3.jpg"></td>
        </tr>
        <tr>
          <td align="center"><table cellpadding="0" cellspacing="0" border="0" width="120">
              <tbody>
                <tr>
                  <td align="center" bgcolor="#002060" style="color:#fff;padding:1px 0; font-size:13px;">「森彦」のオーガニックコーヒー</td>
                </tr>
              </tbody>
            </table></td>
        </tr>
        <tr>
          <td align="center" style="padding:8px 10px; font-size:13px;">「モリヒコ」のオーガニックコーヒーと映画。<br>生まれ変わったディノスシネマズ札幌劇場は<br>こだわりのあるモノであふれた上質な時間を楽しむ場所。<br><span style="color:red;">※映画をご覧にならないお客様でもご利用いただけます。</span></td>
        </tr>

        <tr>
          <td align="center"><table cellpadding="0" cellspacing="0" border="0" width="120">
              <tbody>
                <tr>
                  <td align="center" style="padding:8px 10px; font-size:13px;"><a href="http://cinema.sugai-dinos.jp/sapporo/c3/index.html" target="_blank">⇒詳細はこちら</a></td>
                </tr>
              </tbody>
            </table></td>
        </tr>
      </tbody>
    </table></td>
</tr>
<tr>
  <td>&nbsp;</td>
</tr>
<!--　END オーガニックコーヒー -->