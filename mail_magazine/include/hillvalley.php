<tr>
  <td align="center" style="font-size:17px;font-weight:bold;padding:10px 0 15px;"> 新メニュー登場！<br> ＜HILL VALLEY/ヒルバレー＞<br> グルメポップコーン好評販売中！</td>
</tr>
<tr>
  <td style="border:1px solid #666;"><table cellpadding="0" cellspacing="0" border="0" width="420">
      <tbody>
        <tr>
          <td align="center" bgcolor="#002060" style="color:#fff;padding:7px 0 6px;font-size:17px;"><strong>常識をくつがえす新食感！！！<br>驚きのグルメポップコーン</strong></td>
        </tr>
        <tr>
          <td align="center" style="padding:15px 0;"><img src="http://cinema.sugai-dinos.jp/image/contents/49506/20171025110918_5.png"></td>
        </tr>
        <tr>
          <td align="center"><table cellpadding="0" cellspacing="0" border="0" width="120">
              <tbody>
                <tr>
                  <td align="center" bgcolor="#002060" style="color:#fff;padding:1px 0; font-size:13px;">HillValley MIX　ヒルバレーミックス</td>
                </tr>
              </tbody>
            </table></td>
        </tr>
        <tr>
          <td align="center" style="padding:8px 10px; font-size:13px;">“ピュアゴールドキャラメル”と“シカゴチーズ”をミックス。ヒルバレーを代表するだけでなく、グルメポップコーンのシンボル。深みのあるキャラメル味の甘さと、濃厚チーズのしょっぱさ。ヒルバレーならではの味の共演をお楽しみください。</td>
        </tr>
        <tr>
          <td align="center"><table cellpadding="0" cellspacing="0" border="0" width="120">
              <tbody>
                <tr>
                  <td align="center" bgcolor="#002060" style="color:#fff;padding:1px 0; font-size:13px;">Strawberry Fraise　ストロベリーフレーズ</td>
                </tr>
              </tbody>
            </table></td>
        </tr>
        <tr>
          <td align="center" style="padding:8px 10px; font-size:13px;">ドライないちごの食感と、リッチでまろやかな口どけをお楽しみください。</td>
        </tr>
        <tr>
          <td align="center"><table cellpadding="0" cellspacing="0" border="0" width="120">
              <tbody>
                <tr>
                  <td align="center" bgcolor="#002060" style="color:#fff;padding:1px 0; font-size:13px;">販売価格</td>
                </tr>
                <tr>
                  <td align="center" style="padding:8px 10px; font-size:13px;">600円（税込）</td>
                </tr>
                <tr>
                  <td align="center" style="padding:8px 10px; font-size:13px;"><a href="<?php echo $hillvalley_url[$shop]; ?>" target="_blank">⇒詳細はこちら</a></td>
                </tr>
              </tbody>
            </table></td>
        </tr>
      </tbody>
    </table></td>
</tr>
<tr>
  <td>&nbsp;</td>
</tr>