<!--　START ぐる巻きソーセージ -->
<tr>
  <td align="center" style="font-size:17px;font-weight:bold;padding:10px 0 15px;"> 新商品！「ぐる巻きソーセージ」</td>
</tr>
<tr>
  <td style="border:1px solid #666;"><table cellpadding="0" cellspacing="0" border="0" width="420">
      <tbody>
        <tr>
          <td align="center" bgcolor="#002060" style="color:#fff;padding:7px 0 6px;font-size:17px;"><strong>とってもジューシーでボリュームたっぷり、<br>美味しさ大満足の「ぐる巻きソーセージ」、<br>ぜひご賞味ください♪</strong></td>
        </tr>
        <tr>
           <td align="center" style="padding:15px 0;"><img src="https://www.sugai-dinos.jp/cinema/mailmagazine/gurumaki.jpg"></td>
        </tr>
        <tr>
          <td align="center"><table cellpadding="0" cellspacing="0" border="0" width="120">
              <tbody>
                <tr>
                  <td align="center" style="padding:5px 10px 20px; font-size:13px;"><a href="<?php echo $gurumaki_url[$shop]; ?>" target="_blank">⇒詳細はこちら</a></td>
                </tr>
              </tbody>
            </table></td>
        </tr>
      </tbody>
    </table></td>
</tr>
<tr>
  <td>&nbsp;</td>
</tr>
<!--　END ぐる巻きソーセージ -->