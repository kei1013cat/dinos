<!--　START ライザップLINEスタンプ -->
<tr>
  <td style="border:1px solid #666;"><table cellpadding="0" cellspacing="0" border="0" width="420">
      <tbody>
        <tr>
          <td align="center" bgcolor="#002060" style="color:#fff;padding:7px 0 6px;font-size:17px;"><strong>【限定5/28迄】<br>第6弾ライザップLINEスタンプを無料配信</strong></td>
        </tr>
        <tr>
           <td align="center" style="padding:15px 0;"><img src="https://www.sugai-dinos.jp/cinema/mailmagazine/rizap_linestamp.jpg" width="400"></td>
        </tr>
        <tr>
          <td align="center"><table cellpadding="0" cellspacing="0" border="0" width="400">
              <tbody>

                <tr>
                  <td align="center" style="padding:5px 10px 20px; font-size:14px;">
                    大人気キャラクター「うさぎ＆くま100％」×RIZAPのコラボスタンプ【全16種類】が登場！<br>
                    ライザップ公式アカウントを友だち追加するだけで無料でもらえます。<br><br>
                    【期間限定5月1日(火)～5月28日(月)まで】<br>
                    下記URLから今すぐダウンロード！<br><br>
                    <a href="https://goo.gl/EVAB4m" target="_blank">https://goo.gl/EVAB4m</a>
                  </td>
                </tr>
                <tr>
                  <td align="center">
                    <img src="https://www.sugai-dinos.jp/cinema/mailmagazine/qrcode.png" width="240">
                  </td>
                </tr>
              </tbody>
            </table></td>
        </tr>
      </tbody>
    </table></td>
</tr>
<tr>
  <td>&nbsp;</td>
</tr>
<!--　END ライザップLINEスタンプ -->