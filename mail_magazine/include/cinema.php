<tr>
    <td align="center" style="padding:10px 0 15px;"><a href="https://www.sugai-dinos.jp/cinema/<?php echo $shop; ?>/" target="_blank"><img src="https://www.sugai-dinos.jp/cinema/mailmagazine/logo.png" alt="素晴らしき映画人生を送りなさい" /></a></td>
</tr>
<!-- <tr>
  <td align="center" style="padding:10px 0 15px;">映画ファン必見！映画ブログ配信中！！<br>↓↓↓↓↓↓↓↓↓↓↓↓↓↓ <br><a href="http://cinema.sugai-dinos.jp/pc/blog/" target="_blank"><img src="https://www.sugai-dinos.jp/cinema/mailmagazine/top_menu_blog.png" style="padding-top:10px;" alt="ディノスシネマズ" /></a></td>
</tr>
 -->
<tr>
    <td align="center" style="font-size:17px;font-weight:bold;padding:10px 0 0px;">ディノスシネマズ<?php echo $shop_name[$shop]; ?><br />公開予定作品</td>
</tr>
<?php foreach($movie_list[$shop] as $releasedate_tmp => $movie_arr): ?>
<?php //$releasedate = new DateTime($releasedate_tmp); ?>
<?php $releasedate = $releasedate_tmp; ?>
<tr>
    <td>&nbsp;</td>
</tr>
<tr>
    <td align="center" style="padding-bottom:10px;border-top:1px solid #999;border-bottom:1px solid #999;padding-top:10px;">公開日　<?php echo $releasedate; ?></td>
</tr>
<tr>
    <td>&nbsp;</td>
</tr>
<?php foreach($movie_arr as $movie): ?>
<!--　1件分データここから -->
<tr>
    <td style="border:1px solid #666;">
        <table cellpadding="0" cellspacing="0" border="0" width="420">
            <tr>
                <td align="center" bgcolor="#002060" style="color:#fff;padding:7px 0 6px;font-size:17px;"><?php echo $movie['title'];?>
                    <?php if($movie['PG12']): ?>
                    &nbsp;<span style="color:#f00;">【PG12】</span>
                    <?php endif;?>
                    <?php if($movie['R15+']): ?>
                    &nbsp;<span style="color:#f00;">【R15+】</span>
                    <?php endif;?>
                    <?php if($movie['R18+']): ?>
                    &nbsp;<span style="color:#f00;">【R18+】</span>
                    <?php endif;?></td>
            </tr>

            <?php if($movie['subtitle']): ?>
            <tr>
                <td align="center" style="font-size:13px;padding:15px 0 0px;"><?php echo $movie['subtitle']; ?></td>
            </tr>
            <?php endif; ?>

            <tr>
                <td align="center" style="padding:15px 0;"><img style="width:400px;" src="<?php echo $movie['imgurl'];?>" /></td>
            </tr>
            <tr>
                <td align="center">
                    <table cellpadding="0" cellspacing="0" border="0" width="120">
                        <tr>
                            <td align="center" bgcolor="#002060" style="color:#fff;padding:1px 0; font-size:13px;">監督</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" style="padding:8px 10px; font-size:13px;"><?php echo $movie['director'];?></td>
            </tr>
            <tr>
                <td align="center">
                    <table cellpadding="0" cellspacing="0" border="0" width="120">
                        <tr>
                            <td align="center" bgcolor="#002060" style="color:#fff;padding:1px 0; font-size:13px;">出演</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" style="padding:8px 10px; font-size:13px;"><?php echo $movie['Performer'];?></td>
            </tr>
            <tr>
                <td align="center">
                    <table cellpadding="0" cellspacing="0" border="0" width="120">
                        <tr>
                            <td align="center" bgcolor="#002060" style="color:#fff;padding:1px 0; font-size:13px;">公式HP</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" style="padding:8px 10px; font-size:13px;"><a href="<?php echo $movie['hp'];?>" target="_blank">⇒公式HPはこちら</a></td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td>&nbsp;</td>
</tr>
<!--　1件分データここまで -->
<?php endforeach; ?>
<?php endforeach; ?>
<tr>
    <td bgcolor="#e5004e" align="center" style="padding:10px 0;"><a href="http://www.dinoscinemas-reserve.jp/<?php echo $shop; ?>/schedule/" style="color:#fff;text-decoration:none;font-size:17px;" target="_blank"> 上映スケジュール・ネット予約</a></td>
</tr>
<tr>
    <td>&nbsp;</td>
</tr>
