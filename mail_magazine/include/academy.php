<!--　START アカデミー賞受賞作 -->
<tr>
  <td style="border:1px solid #666;"><table cellpadding="0" cellspacing="0" border="0" width="420">
      <tbody>
        <tr>
          <td align="center" bgcolor="#002060" style="color:#fff;padding:7px 0 6px;font-size:17px;"><strong>本年度アカデミー賞受賞作の上映スケジュール！</strong></td>
        </tr>
        <tr>
          <td align="center" style="padding:15px 0;"><img src="http://cinema.sugai-dinos.jp/image/cinema_detail/1429_1_cher300_20171214154935.jpg"></td>
        </tr>
        <tr>
          <td align="center"  style="padding:8px 10px; font-size:13px;">メイキャップ＆ヘアスタイリング賞で辻一弘さんが日本人初の<br>受賞で話題の、『ウィンストン・チャーチル  ヒトラーから世界を<br>救った男』は、3/30（金）より当劇場のみで上映となります！</td>
        </tr>
        <tr>
          <td align="center"><table cellpadding="0" cellspacing="0" border="0" width="260">
              <tbody>
                <tr>
                  <td align="center" style="padding:15px 10px; font-size:13px;"><a href="http://cinema.sugai-dinos.jp/pc/sapporo/topics/detail.php?id=14537" target="_blank">その他の受賞作のスケジュ－ルはこちら</a></td>
                </tr>
              </tbody>
            </table></td>
        </tr>
      </tbody>
    </table></td>
</tr>
<tr>
  <td>&nbsp;</td>
</tr>
<!--　END アカデミー賞受賞作 -->