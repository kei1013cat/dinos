<!--　START ポップコーンＢＯＸ -->
<tr>
  <td style="border:1px solid #666;"><table cellpadding="0" cellspacing="0" border="0" width="420">
      <tbody>
        <tr>
          <td align="center" bgcolor="#002060" style="color:#fff;padding:7px 0 6px;font-size:17px;"><strong>3/3公開記念！<br>映画館でしか買えないポップコーンＢＯＸ！</strong></td>
        </tr>
        <tr>
          <td align="center" style="padding:15px 0;"><img src="https://www.sugai-dinos.jp/cinema/mailmagazine/dora.jpg"></td>
        </tr>


        <tr>
          <td align="center"><table cellpadding="0" cellspacing="0" border="0" width="120">
              <tbody>
                <tr>
                  <td align="center" bgcolor="#002060" style="color:#fff;padding:1px 0; font-size:13px;">アイスドリンク<br>(M)セット</td>
                </tr>
              </tbody>
            </table></td>
        </tr>
        <tr>
          <td align="center" style="padding:8px 10px; font-size:13px;">かわいいフィギュアストラップ付きです。<br>※数量限定です。無くなり次第終了となります。</td>
        </tr>

        <tr>
          <td align="center"><table cellpadding="0" cellspacing="0" border="0" width="120">
              <tbody>
                <tr>
                  <td align="center" bgcolor="#002060" style="color:#fff;padding:1px 0; font-size:13px;">販売価格</td>
                </tr>
                <tr>
                  <td align="center" style="padding:8px 5px; font-size:13px;">1,100円（税込）</td>
                </tr>
              </tbody>
            </table></td>
        </tr>
      </tbody>
    </table></td>
</tr>
<tr>
  <td>&nbsp;</td>
</tr>
<!--　END ポップコーンＢＯＸ -->