  <!--　START ゴディバ -->
  <tr>
    <td style="border:1px solid #666;"><table cellpadding="0" cellspacing="0" border="0" width="420">
        <tbody>
          <tr>
            <td align="center" bgcolor="#002060" style="color:#fff;padding:7px 0 6px;font-size:17px;"><strong>【<?php echo $shop_name[$shop]; ?>】プレミアムソフトクリーム<br>CREMIA 《クレミア》 好評販売中！</strong></td>
          </tr>
          <tr>
            <td align="center" style="padding:15px 0;" ><img width="400" src="https://www.sugai-dinos.jp/cinema/mailmagazine/20160912142411_1.jpg"></td>
          </tr>
          <tr>
            <td align="center"><table cellpadding="0" cellspacing="0" border="0" width="120">
                <tbody>
                  <tr>
                    <td align="center" bgcolor="#002060" style="color:#fff;padding:1px 0; font-size:13px;">プレミアムソフトクリーム</td>
                  </tr>
                </tbody>
              </table></td>
          </tr>
          <tr>
            <td align="center" style="padding:8px 10px; font-size:13px;">「ソフトクリームを上質なスイーツへ」をコンセプトに生クリーム25％配合・乳脂肪分12.5％の濃厚クリームと上質なスイーツにふさわしい食感のラングドシャコーン。<br>
“プリーツが施されたシルク”をイメージしたクリームの形状を組み合わせた「CREMIA（クレミア）」をお楽しみください。</td>
          </tr>
          <tr>
            <td align="center"><table cellpadding="0" cellspacing="0" border="0" width="120">
                <tbody>
                  <tr>
                    <td align="center" bgcolor="#002060" style="color:#fff;padding:1px 0; font-size:13px;">販売価格</td>
                  </tr>
                  <tr>
                    <td align="center" style="padding:8px 10px; font-size:13px;"><?php echo $cremiasoftcream_amt[$shop]; ?>円（税込）</td>
                  </tr>
                </tbody>
              </table></td>
          </tr>
          <tr>
            <td align="center" style="padding:8px 10px; font-size:13px;"><a href="<?php echo $cremiasoftcream_url[$shop]; ?>" target="_blank">⇒詳細はこちら</a></td>
          </tr>
        </tbody>
      </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <!--　END ゴディバ -->