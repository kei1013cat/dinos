  <tr>
    <td align="center" style="font-size:17px;font-weight:bold;padding:10px 0 15px;">【新発売】<br>ゴディバホットショコリキサー<br>好評販売中！</td>
  </tr>
  <tr>
    <td style="border:1px solid #666;"><table cellpadding="0" cellspacing="0" border="0" width="420">
        <tbody>
          <tr>
            <td align="center" bgcolor="#002060" style="color:#fff;padding:7px 0 6px;font-size:17px;"><strong>GODIVA ホットショコリキサー</strong>
            </td>
          </tr>
          <?php if(in_array($shop,array('tomakomai'))):?>
          <tr>
            <td align="center" style="padding:8px 10px; font-size:15px;">
            <br>苫小牧市内でショコリキサーを飲めるのは<br>ディノスシネマズ苫小牧のみ
            </td>
          </tr>
          <?php endif; ?>
          <tr>
            <td align="center" style="padding:15px 0;"><img src="http://cinema.sugai-dinos.jp/image/contents/50103/20171114123752_1.jpg" width="300"></td>
          </tr>
          <tr>
            <td align="center"><table cellpadding="0" cellspacing="0" border="0" width="120">
                <tbody>
                  <tr>
                    <td align="center" bgcolor="#002060" style="color:#fff;padding:1px 0; font-size:13px;">ゴディバ ホットショコリキサー</td>
                  </tr>
                </tbody>
              </table></td>
          </tr>
          <tr>
            <td align="center" style="padding:8px 10px; font-size:13px;">
            濃厚な72% ダークチョコレートに、カカオが上品に香る、ゴディバならではのホットチョコレートドリンク。
          </td>

          </tr>
          
          <tr>
            <td align="center"><table cellpadding="0" cellspacing="0" border="0" width="120">
                <tbody>
                  <tr>
                    <td align="center" bgcolor="#002060" style="color:#fff;padding:1px 0; font-size:13px;">販売価格</td>
                  </tr>
                  <tr>
                    <td align="center" style="padding:8px 10px; font-size:13px;">550円（税込）</td>
                  </tr>
                  <tr>
                    <td align="center" style="padding:8px 10px; font-size:13px;"><a href="<?php echo $chocolixir_url[$shop]; ?>" target="_blank">⇒詳細はこちら</a></td>
                  </tr>
                </tbody>
              </table></td>
          </tr>
        </tbody>
      </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>