  <!--　START シネマスタンプラリー -->
  <tr>
    <td align="center" style="font-size:17px;font-weight:bold;padding:10px 0 15px;">シネマスタンプラリー実施中！</td>
  </tr>
  <tr>
    <td style="border:1px solid #666;"><table cellpadding="0" cellspacing="0" border="0" width="420">
        <tbody>
          <tr>
            <td align="center" bgcolor="#002060" style="color:#fff;padding:7px 0 6px;font-size:17px;"><strong>スタンプを集めて<br>映画１カ月見放題や招待券を当てよう！</strong>
            </td>
          </tr>
          <tr>
            <td align="center" style="padding:15px 0;"><img src="http://cinema.sugai-dinos.jp/image/contents/50195/20171117130117_2.jpg" width="300"></td>
          </tr>
          <tr>
            <td align="center"><table cellpadding="0" cellspacing="0" border="0" width="120">
                <tbody>
                  <tr>
                    <td align="center" bgcolor="#002060" style="color:#fff;padding:1px 0; font-size:13px;">実施期間</td>
                  </tr>
                </tbody>
              </table></td>
          </tr>
          <tr>
            <td align="center" style="padding:8px 10px; font-size:13px;">
            2017年11月23日（木祝）～2018年1月15日（月）まで
            </td>
          </tr>

          <tr>
            <td align="center"><table cellpadding="0" cellspacing="0" border="0" width="120">
                <tbody>
                  <tr>
                    <td align="center" bgcolor="#002060" style="color:#fff;padding:1px 0; font-size:13px;">応募内容<br>チャンス１</td>
                  </tr>
                </tbody>
              </table></td>
          </tr>
          <tr>
            <td align="center" style="padding:8px 10px; font-size:13px;">
            ディノスシネマズチェーンで３回映画をご覧いただいた方に、抽選で４地区合計１５名様に、20１8年3月中有効の１ヶ月フリーパス（*上限30作品）をプレゼント！
            </td>
          </tr>

          <tr>
            <td align="center"><table cellpadding="0" cellspacing="0" border="0" width="120">
                <tbody>
                  <tr>
                    <td align="center" bgcolor="#002060" style="color:#fff;padding:1px 0; font-size:13px;">応募内容<br>チャンス2</td>
                  </tr>
                </tbody>
              </table></td>
          </tr>
          <tr>
            <td align="center" style="padding:8px 10px; font-size:13px;">
            期間中、ディノスシネマズチェーンで１回以上映画をご覧いただいた方に、抽選で４地区合計４０名様に、<br> 映画招待券１枚をプレゼント！
            </td>
          </tr>

          <tr>
            <td align="center"><table cellpadding="0" cellspacing="0" border="0" width="120">
                <tbody>
                  <tr>
                    <td align="center" bgcolor="#002060" style="color:#fff;padding:1px 0; font-size:13px;">当選発表</td>
                  </tr>
                </tbody>
              </table></td>
          </tr>
          <tr>
            <td align="center" style="padding:8px 10px; font-size:13px;">
            2018年1月20日（土）<br>スガイディノスウェブサイトにて発表します。<br>※フリーパス当選者の方にはご本人様宛てにハガキでもお知らせ致します。 
            </td>
          </tr>




          <tr>
            <td align="center"><table cellpadding="0" cellspacing="0" border="0" width="120">
                <tbody>
                  <tr>
                    <td align="center" style="padding:8px 10px; font-size:13px;"><a href="<?php echo $stamprally_url[$shop]; ?>" target="_blank">⇒詳細はこちら</a></td>
                  </tr>
                </tbody>
              </table></td>
          </tr>
        </tbody>
      </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>

  <!--　END シネマスタンプラリー -->