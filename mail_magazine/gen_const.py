# coding: utf-8
import xlrd
import urllib.request
import html.parser as hp
import re

import sys,io
sys.stdout = io.TextIOWrapper(sys.stdout.buffer, encoding='utf-8') # UTF-8に

#WEBから画像URLを取得するかどうか
getImgUrlflg = 1
excelFile = 'data.xlsx'

global cid_url
global aryHtml
aryHtml = []

###################################
# HTMLParserを継承したクラスを定義
###################################
class Parser(hp.HTMLParser):
    global aryHtml
    def handle_starttag(self, tag, attrs):
        aryHtml.append(tag)
        aryHtml.append(attrs)
    def handle_endtag(self, tag):
        aryHtml.append(tag)
    def handle_data(self, data):
        aryHtml.append(str(data.strip()))

###################################
# WEBからデータを取得して配列にセット
###################################
def getHTML(url):
    # HTMLファイルを開く
    data = urllib.request.urlopen(url)

    # HTMLの取得
    html = data.read()
    html = html.decode('utf-8')

    # HTML解析(タイトルタグの値取得)
    parser = Parser()
    parser.feed(html)

    # 終了処理
    parser.close()
    data.close()

###################################
# スクレイピング解析
# 処理内容：作品名と劇場から画像URLを取得する
###################################
def getImgUrl(title,theater):
    global aryHtml
    global cid_url
    global detail_url
    result = "/"
    #　ディノスシネマズのトップURLをスクレイピング解析
    getHTML("https://www.sugai-dinos.jp/cinema/" + theater + "/coming_soon/")
    march_flg = 0
    for i in range(len(aryHtml)):
    	# 空白行は無視
    	if(len(aryHtml[i])>0):
    		if(aryHtml[i]==title):
    			try:
	    			# 作品名マッチ
	    			march_flg = 1
	    			detail_url = aryHtml[i-15][0][1]
	    			break
    			except:
    				pass
    aryHtml = []
    #　ディノスシネマズの作品詳細ページのスクレイピング解析
    getHTML(detail_url)
    for i in range(len(aryHtml)):
        if(isinstance(aryHtml[i], list)):
            # 空白行は無視
            if(len(aryHtml[i])>0):
            	if(aryHtml[i][0][-1]=="ph"):
            		result = aryHtml[i+2][0][-1]
            		break
    return result

###################################
# Excel日付変換(文字列用)
###################################
def excel_date_week(str):
    return re.sub('(月|火|水|木|金|土|日)', '（' + '\\1' + '）', str)

def excel_date_hyphen(str):
    return re.sub('(-)', '～ ', str)

###################################
# Excel日付変換
###################################
def excel_date(num):
    from datetime import datetime, timedelta
    return(datetime(1899, 12, 30) + timedelta(days=num))

def file_output(date):
	file = open('date.txt', 'w')
	file.write(date)
	file.close()

# Excel読み込み
book = xlrd.open_workbook(excelFile)
sheet = book.sheet_by_index(0)

arys = []
for row_index in range(sheet.nrows):
    for col_index in range(sheet.ncols):
        val = sheet.cell_value(rowx=row_index, colx=col_index)
        if(val!=""):
            arys.append(val)


#********************************************************************************************
# Excelデータを解析
for i in range(len(arys)):

    if(arys[i]=="◆配信："):
        print("<?php $senddate  = new DateTime('" + str(excel_date(arys[i+1]).strftime('%Y-%m-%d')) + "'); ?>")
        print("<?php $movie_list = array(")
        # ファイル出力
        file_output(str(excel_date(arys[i+1]).strftime('%Y%m%d')))

    if(arys[i]=="＜ディノスシネマズ札幌劇場＞"):
        print("'sapporo' => array(")
        theater = 'sapporo'
        date_flg = 0
    if(arys[i]=="＜ディノスシネマズ旭川＞"):
        print("  ),")
        print("),")
        print("'asahikawa' => array(")
        theater = 'asahikawa'
        date_flg = 0
    if(arys[i]=="＜ディノスシネマズ苫小牧＞"):
        print("  ),")
        print("),")
        print("'tomakomai' => array(")
        theater = 'tomakomai'
        date_flg = 0
    if(arys[i]=="＜ディノスシネマズ室蘭＞"):
        print("  ),")
        print("),")
        print("'muroran' => array(")
        theater = 'muroran'
        date_flg = 0
    if(arys[i]=="【PG12】" or arys[i]=="【PG１２】"):
        r12_flg = 1
    if(arys[i]=="【Ｒ１５＋】" or arys[i]=="【R15＋】"):
        r15_flg = 1
    if(arys[i]=="【Ｒ１８＋】" or arys[i]=="【R18＋】"):
        r18_flg = 1
    if(arys[i]=="公開日"):
        if(date_flg==1):
            print("  ),")
        print("  '" + str(excel_date_hyphen(excel_date_week(arys[i+1]))) + "'" + " => array(")
        date_flg = 1
    if(arys[i]=="作品"):
        print("    array(")
        print("      'title' => '" + str(arys[i+1]).strip() + "',")
        # Start 2018.2.7 サブタイトル取得（監督行かどうか判断）
        if(arys[i+2]=="監督"):
            print("      'subtitle' => '',")
        else:
            if(str(arys[i+2]).strip()=="【PG12】" or str(arys[i+2]).strip()=="【PG１２】"):
                print("      'subtitle' => '',")
            elif(str(arys[i+2]).strip()=="【Ｒ１５＋】" or str(arys[i+2]).strip()=="【R15＋】"):
                print("      'subtitle' => '',")
            elif(str(arys[i+2]).strip()=="【Ｒ１８＋】" or str(arys[i+2]).strip()=="【R18＋】"):
                print("      'subtitle' => '',")
            else:
                print("      'subtitle' => '" + str(arys[i+2]).strip() + "',")
        # End 2018.2.7 サブタイトル取得
        if(getImgUrlflg==1):
            img = getImgUrl(str(arys[i+1]).strip(),theater)
        else:
            img = "/"
        print("      'imgurl' => '" + str(img) + "',")

        # R指定初期クリア
        r12_flg = 0
        r15_flg = 0
        r18_flg = 0

    if(arys[i]=="監督"):
        print("      'director' => '" + str(arys[i+1]) + "',")
    if(arys[i]=="出演"):
        print("      'Performer' => '" + str(arys[i+1]) + "',")
    if(arys[i]=="公式ＨＰ"):
        print("      'hp' => '" + str(arys[i+1]) + "',")

        if(r12_flg==1):
            print("      'PG12' => true,")
        else:
            print("      'PG12' => false,")
        if(r15_flg==1):
            print("      'R15+' => true,")
        else:
            print("      'R15+' => false,")
        if(r18_flg==1):
            print("      'R18+' => true,")
        else:
            print("      'R18+' => false,")
        print("    ),")

print("  ),")
print("),")
print("); ?>")
#********************************************************************************************
