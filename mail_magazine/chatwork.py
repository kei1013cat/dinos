#coding=UTF-8
import requests

def get_date():
	fin=open('date.txt')
	line=fin.read()
	fin.close()
	return line

BASE_URL = 'https://api.chatwork.com/v2'
 
#Setting
#roomid   = '79525364' #ルームIDを記載
roomid   = '89287311' #ルームIDを記載
apikey   = 'ce93ccdd23f5efae11aecdc4d9385fcf' #APIのKeyを記載

FILE_DATE = get_date()

message  = '◆' + FILE_DATE[0:4] + "/" + str(FILE_DATE[4:6]) + "/" + str(FILE_DATE[6:8]) + "配信分" + "\nhttp://hearty-sapporo.sakura.ne.jp/cinema-test/" + FILE_DATE + "/sapporo.html\n" + "http://hearty-sapporo.sakura.ne.jp/cinema-test/" + FILE_DATE + "/asahikawa.html\n" + "http://hearty-sapporo.sakura.ne.jp/cinema-test/" + FILE_DATE + "/tomakomai.html\n" + "http://hearty-sapporo.sakura.ne.jp/cinema-test/" + FILE_DATE + "/muroran.html\n"

post_message_url = '{}/rooms/{}/messages'.format(BASE_URL, roomid)

headers = { 'X-ChatWorkToken': apikey}
params = { 'body': message }
r = requests.post(post_message_url,
                    headers=headers,
                    params=params)
print(r)