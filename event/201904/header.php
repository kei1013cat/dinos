<header id="top">
    <div class="pcmenu cf">
        <div class="wrapper">
            <h1><a href="https://www.sugai-dinos.jp/park/" target="_blank"><img src="<?php echo $rootpath;?>images/header_logo.svg" alt="ディノスパーク"></a></h1>

            <nav>
                <ul class="cf">
                    <li><a href="<?php echo $rootpath;?>#teine">札幌手稲店</a></li>
                    <li><a href="<?php echo $rootpath;?>#chitose">ちとせモール店</a></li>
                    <li><a href="<?php echo $rootpath;?>#obihiro">帯広店</a></li>
                    <li><a href="<?php echo $rootpath;?>#otofuke">音更店</a></li>
                    <li><a href="<?php echo $rootpath;?>#muroran">室蘭中央店</a></li>
                </ul>
            </nav>
        </div>
        <!-- wrapper -->
    </div>
    <!-- pcmenu -->
    <div class="spmenu drawermenu" role="banner" id="top">
        <h2><a href="https://www.sugai-dinos.jp/park/" target="_blank"><img src="<?php echo $rootpath;?>images/header_logo.svg" alt="ディノスパーク"></a></h2>
        <button type="button" class="drawer-toggle drawer-hamburger"><span class="sr-only">toggle navigation</span> <span class="drawer-hamburger-icon"></span> </button>
        <nav class="drawer-nav" role="navigation">
            <div class="inner">
                <ul class="drawer-menu">
                    <li><a class="drawer-menu-item" href="<?php echo $rootpath;?>#top">TOP</a></li>
                    <li><a class="drawer-menu-item" href="<?php echo $rootpath;?>#teine">札幌手稲店</a></li>
                    <li><a class="drawer-menu-item" href="<?php echo $rootpath;?>#chitose">ちとせモール店</a></li>
                    <li><a class="drawer-menu-item" href="<?php echo $rootpath;?>#obihiro">帯広店</a></li>
                    <li><a class="drawer-menu-item" href="<?php echo $rootpath;?>#otofuke">音更店</a></li>
                    <li><a class="drawer-menu-item" href="<?php echo $rootpath;?>#muroran">室蘭中央店</a></li>
                </ul>
            </div>
            <!--inner -->
        </nav>
    </div>
    <!-- spmenu -->

</header>
<main role="main">
