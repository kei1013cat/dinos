<?php
$rootpath = "./";
$title = "ディノスパーク｜イベント開催中！";
$description = "北海道を代表するゲームセンターのディノスパークがこの春、5店舗をリニューアル！最新のVRゲームや話題の音ゲーなどが続々登場！また、リニューアル記念として無料で参加できる楽しいイベントも盛りだくさん！";
?>
<!DOCTYPE html>
<html lang="ja">

<head>
    <?php include_once $rootpath."head.php"; ?>

    <meta property="og:type" content="website" />
    <meta property="og:title" content="<?php echo $title; ?>" />
    <meta property="og:url" content="http://www.sugai-dinos.jp/event/201904" />
    <meta property="og:image" content="http://www.sugai-dinos.jp/event/201904/images/ogp.jpg" />
    <meta property="og:description" content="<?php echo $description; ?>" />
    <meta property="og:locale" content="ja_JP" />
    <meta property="og:site_name" content="" />

</head>

<body id="page_index" class="drawer drawer--right">
    <div id="outer">
        <?php include_once $rootpath."header.php"; ?>
        <div id="contents">
            <section class="mainimg" id="top">
                <div class="mainimg_bg">
                    <div class="flag_obi"></div><!-- flag -->
                    <div class="wrapper">
                        <div class="main_text enter-top pb"><img src="<?php echo $rootpath;?>images/mainimg_text.png">
                        </div>
                    </div>
                </div>
            </section>
            <!-- mainimg -->

            <section class="family_event pt pb">
                <div class="wrapper">
                    <h2 class="enter-bottom"><img class="img_center" src="<?php echo $rootpath;?>images/family_event.png" alt="家族みんなで楽しめるイベント開催中"></h2>
                </div>
                <!-- wrapper -->
            </section>
            <!-- family_event -->


            <section id="teine">

                <section class="shop_title pt pb_l">
                    <div class="wrapper">
                        <img class="enter-bottom pb_l" src="<?php echo $rootpath;?>images/shop_teine.png">
                    </div>
                    <!-- wrapper -->
                </section>
                <!-- shop_title -->

                <section class="event">
                    <div class="wrapper">
                        <img class="arrow img_center enter-top" src="<?php echo $rootpath;?>images/under_arrow.svg">
                        <div class="outer">
                            <h3><img class="img_center enter-top pb" src="<?php echo $rootpath;?>images/teine_title.png"></h3>

                            <ul>
                                <li class="enter-bottom">
                                    <h4 class="headline3"><span class="line">でぶねこ「茶トラ」が遊びに来るよ!</span></h4>
                                    <h5>写真撮影 OK! </h5>
                                    <dl class="cf">
                                        <dt><span class="box">開催期間</span></dt>
                                        <dd>4/13（土）・14（日）</dd>
                                    </dl>
                                    <dl class="cf">
                                        <dt><span class="box">開催時間</span></dt>
                                        <dd>15:00と17:00</dd>
                                    </dl>
                                    <p class="kome">※時間は変更になる場合がございます。</p>
                                </li>
                                <li class="enter-bottom">
                                    <h4 class="headline3"><span class="line">太鼓の達人交流会</span></h4>
                                    <h5>ゲスト:太鼓の芸人やまだ君</h5>
                                    <dl class="cf">
                                        <dt><span class="box">開催期間</span></dt>
                                        <dd>4/13（土）・14（日）</dd>
                                    </dl>
                                    <dl class="cf">
                                        <dt><span class="box">開催時間</span></dt>
                                        <dd>14:00</dd>
                                    </dl>
                                </li>
                                <li class="enter-bottom">
                                    <h4 class="headline3"><span class="line">リニューアル記念！大ビンゴ大会</span></h4>
                                    <h5>参加無料！何が当たるかお楽しみ♪</h5>
                                    <dl class="cf">
                                        <dt><span class="box">開催期間</span></dt>
                                        <dd>土・日・祝</dd>
                                    </dl>
                                    <dl class="cf">
                                        <dt><span class="box">開催時間</span></dt>
                                        <dd>16:00</dd>
                                    </dl>
                                </li>
                                <li class="enter-bottom">
                                    <h4 class="headline3"><span class="line">お菓子スプラッシュ</span></h4>
                                    <h5>参加無料！お菓子が宙を舞う？！</h5>
                                    <dl class="cf">
                                        <dt><span class="box">開催期間</span></dt>
                                        <dd>土・日・祝</dd>
                                    </dl>
                                    <dl class="cf">
                                        <dt><span class="box">開催時間</span></dt>
                                        <dd>14:00</dd>
                                    </dl>
                                </li>
                                <li class="enter-bottom">
                                    <h4 class="headline3"><span class="line">巨大ガラポン抽選会</span></h4>
                                    <h5>テンションMAXの巨大ガラポン！</h5>
                                    <dl class="cf">
                                        <dt><span class="box">開催期間</span></dt>
                                        <dd>毎日開催</dd>
                                    </dl>
                                    <dl class="cf">
                                        <dt><span class="box">開催時間</span></dt>
                                        <dd>店舗へお問い合わせください。</dd>
                                    </dl>
                                </li>
                                <li class="enter-bottom">
                                    <h4 class="headline3"><span class="line">プリクラ全台200円</span></h4>
                                    <h5>キュートな自撮りいっぱいしよ♪</h5>
                                    <dl class="cf">
                                        <dt><span class="box">開催期間</span></dt>
                                        <dd>毎日開催（期間限定）</dd>
                                    </dl>
                                    <dl class="cf">
                                        <dt><span class="box">開催時間</span></dt>
                                        <dd>店舗へお問い合わせください</dd>
                                    </dl>
                                </li>
                            </ul>
                            <div class="twitter pt_s pb_l enter-bottom">
                                <p class="kome1 pb_s">※4/4～4/8リニューアルの為、休業させて頂きます。</p>
                                <p class="linkbtn"><a href="https://twitter.com/SD_TEINE" target="_blank">札幌手稲店Twitterはこちら</a></p>
                            </div>
                        </div>
                        <!-- outer -->
                    </div>
                    <!-- wrapper -->
                </section>
                <!-- event -->
            </section>
            <!-- teine -->

            <section id="chitose">

                <section class="shop_title pt_l pb_l">
                    <div class="wrapper">
                        <img class="enter-bottom pb_l" src="<?php echo $rootpath;?>images/shop_chitose.png">
                    </div>
                    <!-- wrapper -->
                </section>
                <!-- shop_title -->

                <section class="event">
                    <div class="wrapper">
                        <img class="arrow img_center enter-top" src="<?php echo $rootpath;?>images/under_arrow.svg">
                        <div class="outer">
                            <h3><img class="img_center enter-top pb" src="<?php echo $rootpath;?>images/chitose_title.png"></h3>

                            <ul>
                                <li class="enter-bottom">
                                    <h4 class="headline3"><span class="line">BOXティッシュプレゼント！</span></h4>
                                    <h5>各日・先着300名様</h5>
                                    <dl class="cf">
                                        <dt><span class="box">開催期間</span></dt>
                                        <dd>4/13（土）・14（日）</dd>
                                    </dl>
                                    <dl class="cf">
                                        <dt><span class="box">開催時間</span></dt>
                                        <dd>オープンより</dd>
                                    </dl>
                                </li>
                                <li class="enter-bottom">
                                    <h4 class="headline3"><span class="line">巨大ガラポン毎日開催！</span></h4>
                                    <h5>豪華景品が当たる！</h5>
                                    <dl class="cf">
                                        <dt><span class="box">開催期間</span></dt>
                                        <dd>毎日</dd>
                                    </dl>
                                    <dl class="cf">
                                        <dt><span class="box">開催時間</span></dt>
                                        <dd>店舗へお問い合わせください</dd>
                                    </dl>
                                    <p class="kome">※お一人様1日1回限定</p>
                                </li>
                                <li class="enter-bottom">
                                    <h4 class="headline3"><span class="line">キッズカード大会</span></h4>
                                    <h5>人気のタイトル盛りだくさん！</h5>
                                    <dl class="cf">
                                        <dt><span class="box">開催期間</span></dt>
                                        <dd>毎週／土曜日・日曜日</dd>
                                    </dl>
                                    <dl class="cf">
                                        <dt><span class="box">開催時間</span></dt>
                                        <dd>店舗へお問い合わせください</dd>
                                    </dl>
                                </li>

                            </ul>
                            <div class="twitter pt_s pb_l enter-bottom">
                                <p class="linkbtn"><a href="https://twitter.com/SD_DinosParkTM" target="_blank">ちとせモール店Twitterはこちら</a></p>
                            </div>
                        </div>
                        <!-- outer -->
                    </div>
                    <!-- wrapper -->
                </section>
                <!-- event -->
            </section>
            <!-- teine -->

            <section id="obihiro">

                <section class="shop_title pt_l pb_l">
                    <div class="wrapper">
                        <img class="enter-bottom pb_l" src="<?php echo $rootpath;?>images/shop_obihiro.png">
                    </div>
                    <!-- wrapper -->
                </section>
                <!-- shop_title -->

                <section class="event">
                    <div class="wrapper">
                        <img class="arrow img_center enter-top" src="<?php echo $rootpath;?>images/under_arrow.svg">
                        <div class="outer">
                            <h3><img class="img_center enter-top pb" src="<?php echo $rootpath;?>images/obihiro_title.png"></h3>

                            <ul>
                                <li class="enter-bottom">
                                    <h4 class="headline3"><span class="line">大ビンゴ大会</span></h4>
                                    <h5>参加無料！何が当たるかお楽しみ♪</h5>
                                    <dl class="cf">
                                        <dt><span class="box">開催期間</span></dt>
                                        <dd>毎週／土・日・祝</dd>
                                    </dl>
                                    <dl class="cf">
                                        <dt><span class="box">開催時間</span></dt>
                                        <dd>12:00・16:00・21:00</dd>
                                    </dl>
                                </li>
                            </ul>
                            <div class="twitter pt_s pb_l enter-bottom">
                                <p class="linkbtn"><a href="https://twitter.com/Obihiro_sugai" target="_blank">帯広店Twitterはこちら</a></p>
                            </div>
                        </div>
                        <!-- outer -->
                    </div>
                    <!-- wrapper -->
                </section>
                <!-- event -->
            </section>
            <!-- obihiro -->

            <section id="otofuke">

                <section class="shop_title pt_l pb_l">
                    <div class="wrapper">
                        <img class="enter-bottom pb_l" src="<?php echo $rootpath;?>images/shop_otofuke.png">
                    </div>
                    <!-- wrapper -->
                </section>
                <!-- shop_title -->

                <section class="event">
                    <div class="wrapper">
                        <img class="arrow img_center enter-top" src="<?php echo $rootpath;?>images/under_arrow.svg">
                        <div class="outer">
                            <h3><img class="img_center enter-top pb" src="<?php echo $rootpath;?>images/otofuke_title.png"></h3>

                            <ul>
                                <li class="enter-bottom">
                                    <h4 class="headline3"><span class="line">おみくじ付風船無料配布</span></h4>
                                    <h5>お子さま限定！何が当たるかお楽しみ♪</h5>
                                    <dl class="cf">
                                        <dt><span class="box">開催期間</span></dt>
                                        <dd>4/6（土）・7（日）</dd>
                                    </dl>
                                    <dl class="cf">
                                        <dt><span class="box">開催時間</span></dt>
                                        <dd>14:00～　（抽選は14:30）</dd>
                                    </dl>
                                </li>
                                <li class="enter-bottom">
                                    <h4 class="headline3"><span class="line">お菓子プレゼント</span></h4>
                                    <h5>小学生以下限定</h5>
                                    <dl class="cf">
                                        <dt><span class="box">開催期間</span></dt>
                                        <dd>4/6（土）・7（日）</dd>
                                    </dl>
                                    <dl class="cf">
                                        <dt><span class="box">開催時間</span></dt>
                                        <dd>15:00〜</dd>
                                    </dl>
                                    <p class="kome">※無くなり次第終了</p>
                                </li>
                                <li class="enter-bottom">
                                    <h4 class="headline3"><span class="line">リニューアル記念！大ビンゴ大会</span></h4>
                                    <h5>参加無料！何が当たるかお楽しみ♪</h5>
                                    <dl class="cf">
                                        <dt><span class="box">開催期間</span></dt>
                                        <dd>4/6（土）・7（日）</dd>
                                    </dl>
                                    <dl class="cf">
                                        <dt><span class="box">開催時間</span></dt>
                                        <dd>15:30～</dd>
                                    </dl>
                                </li>
                                <li class="enter-bottom">
                                    <h4 class="headline3"><span class="line">お菓子スプラッシュ</span></h4>
                                    <h5>参加無料！何が当たるかお楽しみ♪</h5>
                                    <dl class="cf">
                                        <dt><span class="box">開催期間</span></dt>
                                        <dd>4/6（土）・7（日）</dd>
                                    </dl>
                                    <dl class="cf">
                                        <dt><span class="box">開催時間</span></dt>
                                        <dd>16:20～</dd>
                                    </dl>
                                </li>
                            </ul>
                            <div class="twitter pt_s pb_l enter-bottom">
                                <p class="linkbtn"><a href="https://twitter.com/otofuke_sugai" target="_blank">音更店Twitterはこちら</a></p>
                            </div>
                        </div>
                        <!-- outer -->
                    </div>
                    <!-- wrapper -->
                </section>
                <!-- event -->
            </section>
            <!-- obihiro -->

            <section id="muroran">

                <section class="shop_title pt_l pb_l">
                    <div class="wrapper">
                        <img class="enter-bottom pb_l" src="<?php echo $rootpath;?>images/shop_muroran.png">
                    </div>
                    <!-- wrapper -->
                </section>
                <!-- shop_title -->

                <section class="event">
                    <div class="wrapper">
                        <img class="arrow img_center enter-top" src="<?php echo $rootpath;?>images/under_arrow.svg">
                        <div class="outer">
                            <h3><img class="img_center enter-top pb" src="<?php echo $rootpath;?>images/muroran_title.png"></h3>

                            <ul>
                                <li class="enter-bottom">
                                    <h4 class="headline3"><span class="line">ビンゴ大会</span></h4>
                                    <h5>何が当たるかお楽しみ♪</h5>
                                    <dl class="cf">
                                        <dt><span class="box">開催期間</span></dt>
                                        <dd>毎週日曜日</dd>
                                    </dl>
                                    <dl class="cf">
                                        <dt><span class="box">開催時間</span></dt>
                                        <dd>店舗へお問い合わせください</dd>
                                    </dl>
                                </li>
                            </ul>
                            <div class="twitter pt_s pb_l enter-bottom">
                                <p class="linkbtn"><a href="https://twitter.com/dpmrtj" target="_blank">室蘭中央店Twitterはこちら</a></p>
                            </div>
                        </div>
                        <!-- outer -->
                    </div>
                    <!-- wrapper -->
                </section>
                <!-- event -->
            </section>
            <!-- muroran -->
        </div>
        <!-- contents -->

        <?php include_once $rootpath."footer.php"; ?>
    </div>
    <!-- outer -->

</body>

</html>
