<?php date_default_timezone_set('Asia/Tokyo'); ?>
<footer>

    <div class="footer-outer cf">
        <p class="logo"><a href="https://www.sugai-dinos.jp/park/" target="_blank"><img src="<?php echo $rootpath;?>images/footer_logo.svg" alt="ディノスパーク"></a></p>
        <!-- logo -->
        <p class="copy">&copy; <?php echo date("Y"); ?> SUGAI DINOS. All Rights Reserved.</p>
    </div>
    <!-- outer -->


</footer>
</main>
