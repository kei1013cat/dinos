<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width">
<meta name="description" content="<?php echo $description; ?>" />
<meta name="keywords" content="ディノスパーク,リニューアル,ゲームセンター,イベント開催,札幌手稲店,ちとせモール店,帯広店,音更店,室蘭中央店" />
<meta name="format-detection" content="telephome=no">
<link href="https://www.sugai-dinos.jp/pc/img/favicon.ico" type="image/x-icon" rel="icon" />
<title><?php echo $title; ?></title>
<link rel="stylesheet" href="<?php echo $rootpath;?>css/import.css">
<script src="<?php echo $rootpath;?>js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="<?php echo $rootpath;?>js/smoothScroll.js"></script>
<script src="<?php echo $rootpath;?>js/iscroll.min.js"></script>
<link rel="stylesheet" href="<?php echo $rootpath;?>js/drawer/dist/css/drawer.min.css">
<script type="text/javascript" src="<?php echo $rootpath;?>js/drawer/dist/js/drawer.min.js"></script>

<!-- scrollreveal -->
<script type="text/javascript" src="<?php echo $rootpath;?>js/scrollreveal/scrollreveal.js"></script>
<script type="text/javascript" src="<?php echo $rootpath;?>js/scrollreveal/scrollreveal.thema.js"></script>

<script>
    $(document).ready(function() {
        $('.drawer').drawer();
    });

</script>

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-WN66JF" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>
    (function(w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({
            'gtm.start': new Date().getTime(),
            event: 'gtm.js'
        });
        var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s),
            dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src =
            '//www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-WN66JF');

</script>
<!-- End Google Tag Manager -->

<?php

//-------------------------------------------
// スマホならtrue, タブレット・PCならfalseを返す
//-------------------------------------------

global $is_mobile;
$is_mobile = false;

$useragents = array(
 'iPhone',          // iPhone
 'iPod',            // iPod touch
 'Android',         // 1.5+ Android
 'dream',           // Pre 1.5 Android
 'CUPCAKE',         // 1.5+ Android
 'blackberry9500',  // Storm
 'blackberry9530',  // Storm
 'blackberry9520',  // Storm v2
 'blackberry9550',  // Storm v2
 'blackberry9800',  // Torch
 'webOS',           // Palm Pre Experimental
 'incognito',       // Other iPhone browser
 'webmate'          // Other iPhone browser
 );
 $pattern = '/'.implode('|', $useragents).'/i';
 if( preg_match($pattern, $_SERVER['HTTP_USER_AGENT'])){
     $is_mobile = preg_match($pattern, $_SERVER['HTTP_USER_AGENT']);
 }

function is_mobile(){
    global $is_mobile;
    return $is_mobile;
    //return false;
}
function is_pc(){
    if(is_mobile()){
        return false;
    }else{
        return true;
    }
}
function is_ipad(){
    if(strpos($_SERVER['HTTP_USER_AGENT'],'iPad')){
        return true;
    }else{
        return false;
    }
}
function mobile_img(){
    if (is_mobile()) {
        echo "_sp";
    }
}


?>
